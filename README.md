O objetivo geral deste projeto é apresentar uma proposta de arquitetura para criação do  Projeto Plurianual de Gestão Municipal.


-  **Arquitetura Front-End** -> Angular 
https://gitlab.com/LuisMirard/sgm-front

-  **Arquitetura Back-End** -> Java - Spring Boot             
https://gitlab.com/LuisMirard/sgm-api-geoprocessamento            
https://gitlab.com/LuisMirard/sgm-api-business            
https://gitlab.com/LuisMirard/smg-api-cidadao